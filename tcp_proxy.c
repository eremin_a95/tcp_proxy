#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>

#include "util.h"

#define MAX_EVENTS		32
#define MAX_CONN		128
#define BUF_SIZE		4096
#define TMP_BUF_SIZE	32

#define LOG(...)		fprintf(stdout, __VA_ARGS__)
#define ERR(...)		fprintf(stderr, __VA_ARGS__)

int proxy_sock_create(const struct sockaddr *addr)
{
	int proxy_fd = socket(addr->sa_family, SOCK_STREAM, 0);
	if (proxy_fd < 0)
		return -1;

	if (connect(proxy_fd, addr, sizeof(*addr)) < 0) {
		close(proxy_fd);
		return -1;
	}

	return proxy_fd;
}

#define CONN_CTX_CLEANUP	1

struct conn_ctx {
	struct conn_ctx *next; /* cleanup list */
	unsigned int flags;
	int epoll_fd;
	int peer_fd;
	int proxy_fd;
	struct sockaddr peer_addr;
	struct sockaddr proxy_addr;
	char buf[BUF_SIZE];
};

void conn_create(int epoll_fd, int listen_sock, struct addrinfo *proxy_ai, in_addr_t proxy_port)
{
	struct addrinfo *ai;
	struct sockaddr addr;
	socklen_t addrlen;
	int fd;
	uint32_t flags;
	struct conn_ctx *ctx;
	char peer[TMP_BUF_SIZE];
	char proxy[TMP_BUF_SIZE];

	do {
		addrlen = sizeof(addr);
		fd = accept(listen_sock, &addr, &addrlen);
		if (fd < 0) {
			if ((errno != EAGAIN) && (errno != EWOULDBLOCK))
				ERR("Failed to accept socket: %s\n", strerror(errno));
			break;
		}

		ctx = malloc(sizeof(*ctx));
		if (ctx == NULL) {
			ERR("Not enought memory to create connection context\n");
			goto malloc_err;
		}

		memset(ctx, 0, sizeof(*ctx));
		ctx->epoll_fd = epoll_fd;
		ctx->peer_fd = fd;
		memcpy(&ctx->peer_addr, &addr, addrlen);

		flags = EPOLLIN | EPOLLET | EPOLLRDHUP | EPOLLHUP | EPOLLERR;
		if (epoll_ctl_add(epoll_fd, ctx->peer_fd, ctx, flags) < 0) {
			ERR("Failed to add peer fd epoll: %s\n", strerror(errno));
			goto epoll_add_err;
		}

		for (ai = proxy_ai; ai != NULL; ai = ai->ai_next) {
			memcpy(&ctx->proxy_addr, ai->ai_addr, sizeof(ctx->proxy_addr));
			sockaddr_set_port(&ctx->proxy_addr, proxy_port);
			ctx->proxy_fd = proxy_sock_create(&ctx->proxy_addr);
			if (ctx->proxy_fd >= 0)
				break;
		}
		if (ctx->proxy_fd < 0) {
			ERR("Failed to create proxy socket\n");
			goto proxy_err;
		}

		if (epoll_ctl_add(epoll_fd, ctx->proxy_fd, ctx, flags) < 0) {
			ERR("Failed to add proxy fd to epoll: %s\n", strerror(errno));
			goto proxy_ctl_err;
		}

		inet_ntop(AF_INET, sockaddr_addr(&ctx->peer_addr), peer, sizeof(ctx->peer_addr));
		inet_ntop(AF_INET, sockaddr_addr(&ctx->proxy_addr), proxy, sizeof(ctx->proxy_addr));
		LOG("New proxy: \t%s:%d\t<---> %s:%d\n",
			peer, ntohs(sockaddr_port(&ctx->peer_addr)),
			proxy, ntohs(sockaddr_port(&ctx->proxy_addr)));
		continue;

proxy_ctl_err:
		close(ctx->proxy_fd);
proxy_err:
		epoll_ctl_del(epoll_fd, ctx->peer_fd);
epoll_add_err:
		free(ctx);
malloc_err:
		close(fd);
	} while (1);
}

void cleanup_list_append(struct conn_ctx **list, struct conn_ctx *ctx)
{
	if (ctx->flags & CONN_CTX_CLEANUP)
		return;
	
	ctx->flags |= CONN_CTX_CLEANUP;
	ctx->next = *list;
	*list = ctx;
}

void conn_close(struct conn_ctx *ctx)
{
	char peer[TMP_BUF_SIZE];
	char proxy[TMP_BUF_SIZE];

	epoll_ctl_del(ctx->epoll_fd, ctx->proxy_fd);
	epoll_ctl_del(ctx->epoll_fd, ctx->peer_fd);
	close(ctx->proxy_fd);
	close(ctx->peer_fd);

	inet_ntop(AF_INET, sockaddr_addr(&ctx->peer_addr), peer, sizeof(ctx->peer_addr));
	inet_ntop(AF_INET, sockaddr_addr(&ctx->proxy_addr), proxy, sizeof(ctx->proxy_addr));
	LOG("Close proxy: \t%s:%d\t<---> %s:%d\n",
		peer, ntohs(sockaddr_port(&ctx->peer_addr)),
		proxy, ntohs(sockaddr_port(&ctx->proxy_addr)));

	free(ctx);
}

int proxy_data(char *buf, int buflen, int fdin, int fdout)
{
	ssize_t rlen, wlen;
	do {
		rlen = read(fdin, buf, buflen);
		if (rlen == 0)
			return -1;
		else if ((rlen < 0) && ((errno == EAGAIN) || (errno == EWOULDBLOCK)))
			break;
		wlen = write(fdout, buf, rlen);
		if (wlen != rlen)
			return -1;
	} while (1);

	return 0;
}

int conn_handle(struct conn_ctx *ctx)
{
	int ret = 0;
	ret += proxy_data(ctx->buf, sizeof(ctx->buf), ctx->peer_fd, ctx->proxy_fd);
	ret += proxy_data(ctx->buf, sizeof(ctx->buf), ctx->proxy_fd, ctx->peer_fd);
	return ret;
}

int listen_sock_create(const struct sockaddr *listen_addr)
{
	char buf[TMP_BUF_SIZE];
	int listen_sock = socket(listen_addr->sa_family, SOCK_STREAM, 0);
	if (listen_sock < 0) {
		ERR("Failed to create listen socket: %s\n", strerror(errno));
		return -1;
	}

	if (bind(listen_sock, listen_addr, sizeof(*listen_addr)) < 0) {
		ERR("Failed to bind listen socket: %s\n", strerror(errno));
		goto err;
	}

	if (listen(listen_sock, MAX_CONN) < 0) {
		ERR("Failed to listen socket: %s\n", strerror(errno));
		goto err;
	}

	inet_ntop(AF_INET, sockaddr_addr(listen_addr), buf, sizeof(*listen_addr));
	LOG("Listening %s:%d\n", buf, ntohs(sockaddr_port(listen_addr)));
	return listen_sock;
err:
	close(listen_sock);
	return -1;
}

void main_loop(int epoll_fd, int listen_sock, struct addrinfo *proxy_ai, in_port_t proxy_port)
{
	struct epoll_event events[MAX_EVENTS];
	struct conn_ctx *cleanup_list = NULL;
	int ret, i, num;

	while (1) {
		num = epoll_wait(epoll_fd, events, MAX_EVENTS, -1);
		for (i = 0; i < num; i++) {
			ret = 0;
			if (events[i].data.ptr == NULL) {
				/* Create new connection */
				conn_create(epoll_fd, listen_sock, proxy_ai, proxy_port);
			} else if (events[i].events & EPOLLIN) {
				/* Proxy data */
				ret = conn_handle(events[i].data.ptr);
			}
			if ((ret != 0) || (events[i].events & (EPOLLRDHUP | EPOLLHUP | EPOLLERR))) {
				/* Add context to cleanup list */
				cleanup_list_append(&cleanup_list, events[i].data.ptr);
			}
		}

		while (cleanup_list != NULL) {
			/* Close connection */
			struct conn_ctx *next = cleanup_list->next;
			conn_close(cleanup_list);
			cleanup_list = next;
		}
	}
}

int main(int argc, char **argv)
{
	const char *listen_str = NULL;
	const char *proxy_str = NULL;
	struct addrinfo *listen_ai, *proxy_ai, *ai;
	in_port_t listen_port, proxy_port;
	int listen_sock, epoll_fd;
	int rez;

	while ((rez = getopt(argc, argv, "l:p:")) != -1) {
		switch (rez) {
		case 'l': listen_str = optarg; break;
		case 'p': proxy_str = optarg; break;
		}
	}

	if (listen_str == NULL) {
		ERR("Listen address not defined\n");
		return -1;
	}

	if (proxy_str == NULL) {
		ERR("Proxy address not defined\n");
		return -1;
	}

	if (resolve_addr(listen_str, &listen_ai, &listen_port) < 0) {
		ERR("Failed to resolve listen address '%s'\n", listen_str);
		return -1;
	}

	if (resolve_addr(proxy_str, &proxy_ai, &proxy_port) < 0) {
		ERR("Failed to resolve proxy address '%s'\n", proxy_str);
		return -1;
	}
	
	for (ai = listen_ai; ai != NULL; ai = ai->ai_next) {
		struct sockaddr sa;
		memcpy(&sa, ai->ai_addr, sizeof(sa));
		sockaddr_set_port(&sa, listen_port);
		listen_sock = listen_sock_create(&sa);
		if (listen_sock >= 0)
			break;
	}
	if (listen_sock < 0)
		return -1;

	resolve_addr_release(listen_ai);

	epoll_fd = epoll_create(3);
	if (epoll_fd < 0) {
		ERR("Failed to create epoll: %s\n", strerror(errno));
		return -1;
	}

	if (epoll_ctl_add(epoll_fd, listen_sock, NULL, EPOLLIN | EPOLLET) < 0) {
		ERR("Failed to add epoll: %s\n", strerror(errno));
		return -1;
	}

	main_loop(epoll_fd, listen_sock, proxy_ai, proxy_port);

	resolve_addr_release(proxy_ai);
	close(epoll_fd);
	return 0;
}