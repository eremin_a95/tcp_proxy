BIN=tcp_proxy

CC=gcc
CFLAGS+= -g -Wall -Wextra
SRC= tcp_proxy.c

all: $(BIN)

$(BIN):
	$(CC) $(CFLAGS) -o $@ $(SRC)
	
clean:
	rm -f $(BIN) *.o
