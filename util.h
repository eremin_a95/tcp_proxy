#include <sys/epoll.h>
#include <fcntl.h>
#include <string.h>
#include <netdb.h>

#define DEFAULT_PORT	80

static inline
in_port_t sockaddr_port(const struct sockaddr *sa)
{
	switch (sa->sa_family) {
	case AF_INET:
		return ((const struct sockaddr_in*)sa)->sin_port;
	case AF_INET6:
		return ((const struct sockaddr_in6*)sa)->sin6_port;
	default:
		return 0;
	}
}

static inline
void sockaddr_set_port(struct sockaddr *sa, const in_port_t port)
{
	switch (sa->sa_family) {
	case AF_INET:
		((struct sockaddr_in*)sa)->sin_port = port;
		break;
	case AF_INET6:
		((struct sockaddr_in6*)sa)->sin6_port = port;
		break;
	}
}

static inline
const char* sockaddr_addr(const struct sockaddr *sa)
{
	switch (sa->sa_family) {
	case AF_INET:
		return (const char*)&((const struct sockaddr_in*)sa)->sin_addr;
	case AF_INET6:
		return (const char*)&((const struct sockaddr_in6*)sa)->sin6_addr;
	default:
		return NULL;
	}
}

static inline
int set_non_blocking(int fd)
{
	int flags = fcntl(fd, F_GETFL, 0);
	if (flags < 0)
		return -1;
	if (fcntl(fd, F_SETFL, flags | O_NONBLOCK) == -1)
		return -1;
	return 0;
}

static inline
int epoll_ctl_add(int epoll_fd, int fd, void *ptr, uint32_t events)
{
	struct epoll_event ev;
	ev.events = events;
	ev.data.ptr = ptr;
	if (events & EPOLLET) {
		if (set_non_blocking(fd) < 0) {
			return -1;
		}
	}
	return epoll_ctl(epoll_fd, EPOLL_CTL_ADD, fd, &ev);
}

static inline
int epoll_ctl_del(int epoll_fd, int fd)
{
	return epoll_ctl(epoll_fd, EPOLL_CTL_DEL, fd, NULL);
}

static inline
int resolve_addr(const char *str, struct addrinfo **ai, in_port_t *port)
{
	struct addrinfo hints;
	struct addrinfo *result;
	char *dup = strdup(str);
	char *dots = strstr(dup, ":");
	if (dots == NULL) {
		*port = htons(DEFAULT_PORT);
	} else {
		*port = htons(atoi(dots+1));
		*dots = '\0';
	}

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;     /* Allow IPv4 or IPv6 */
	hints.ai_socktype = SOCK_STREAM; /* Datagram socket */

	int ret = getaddrinfo(dup, NULL, &hints, &result);
	free(dup);
	if (ret != 0)
		return -1;

	*ai = result;
	return 0;
}

static inline
void resolve_addr_release(struct addrinfo *ai)
{
	freeaddrinfo(ai);
}
